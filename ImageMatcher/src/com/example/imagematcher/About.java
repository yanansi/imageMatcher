/**
 * Assignment 1 : Image Matcher
 * This application is a game that consists of matching 2 images in a 3x3 grid
 * of pictures. There are counters to keep track (score). 
 * 
 * @author Yiming Yan
 * @author Christoffer Baur
 * @author Rafik Ifrani
 * @version 2015-09-27
 * 
 */

package com.example.imagematcher;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class About extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void close(View view){
		setResult(RESULT_OK);
		finish();
	}

}
