/**
 * Assignment 1 : Image Matcher
 * This application is a game that consists of matching 2 images in a 3x3 grid
 * of pictures. There are counters to keep track (score). 
 * 
 * @author Yiming Yan
 * @author Christoffer Baur
 * @author Rafik Ifrani
 * @version 2015-09-27
 * 
 */

package com.example.imagematcher;

import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	//Declaration of class variables
	
	private final static String TAG = "Image Matcher";
	//array of image buttons used for the images to match
	private ImageButton[] imageButtonArray;
	//res id for the back image 
	private int back = R.drawable.back;
	//counters for the game
	private int onGameInvokedCtr = 0;
	private int matchCtr = 0;
	private int missCtr = 0;
	private int lifeMissCtr = 0;
	private int lifeMatchCtr = 0;
	//references to the TextView for the counters
	private TextView matchesCtr;
	private TextView missesCtr;
	private TextView lifeMissesCtrView;
	private TextView lifeMatchesCtrView;
	//all the images non shuffled in res id array
	private int[] rawImageArray = { R.drawable.p0, R.drawable.p1, R.drawable.p2, R.drawable.p3, R.drawable.p4, R.drawable.p5,
			R.drawable.p6, R.drawable.p7, R.drawable.p8, R.drawable.p9, R.drawable.p10};
	//array containing the images to match
	private int[] matchIndex = new int[2];
	//array containing the shuffled images to display in view
	private int[] gameBoardLayout = new int[9];
	//array which verifies if the images are clickable
	private boolean[] isClickable = new boolean[]{true,true,true,true,true,true,true,true,true};

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		// Initialize ImageButton array
		imageButtonArray = new ImageButton[] { (ImageButton) findViewById(R.id.p0), (ImageButton) findViewById(R.id.p1),
				(ImageButton) findViewById(R.id.p2), (ImageButton) findViewById(R.id.p3),
				(ImageButton) findViewById(R.id.p4), (ImageButton) findViewById(R.id.p5),
				(ImageButton) findViewById(R.id.p6), (ImageButton) findViewById(R.id.p7),
				(ImageButton) findViewById(R.id.p8) };
		
		//getting references to the counter views
		matchesCtr = (TextView) findViewById(R.id.matchesCtr);
		missesCtr = (TextView) findViewById(R.id.missesCtr);
		lifeMissesCtrView = (TextView) findViewById(R.id.lifemissesCtr);
		lifeMatchesCtrView = (TextView) findViewById(R.id.lifematchesCtr);

		//if a saved instance exists, then assign values
		if (savedInstanceState != null) {
			super.onRestoreInstanceState(savedInstanceState);
			
			//assigning all the values to the counters and respective text views, and other variables
			onGameInvokedCtr = savedInstanceState.getInt("onGameInvokedCtr");
			matchCtr = savedInstanceState.getInt("matchCtr");
			matchesCtr.setText(matchCtr + "");
			missCtr = savedInstanceState.getInt("missCtr");
			missesCtr.setText(missCtr + "");
			gameBoardLayout = savedInstanceState.getIntArray("gameBoardLayout");
			matchIndex=savedInstanceState.getIntArray("matchIndex");
			isClickable = savedInstanceState.getBooleanArray("isClickable");
			
			//assigning the images to the buttons
			for (int i = 0; i < 9; i++) {
				imageButtonArray[i].setImageResource(gameBoardLayout[i]);
				Log.i(TAG, isClickable[i]+"");
				imageButtonArray[i].setClickable(isClickable[i]);
				imageButtonArray[i].setTag(i);
			}

		} else {
			setupImageLayout();
		}

		// no editor needed we're reading values
		//getting the values for the persistent counters stored
		SharedPreferences prefs = getPreferences(MODE_PRIVATE);
		Log.i(TAG, "b4GetInt/" + "match*miss/" + lifeMatchCtr + "*" + lifeMissCtr);
		lifeMissCtr = prefs.getInt("lifeMissCtr", 0);
		lifeMissesCtrView.setText(lifeMissCtr + "");

		lifeMatchCtr = prefs.getInt("lifeMatchCtr", 0);
		lifeMatchesCtrView.setText(lifeMatchCtr + "");
		Log.i(TAG, "getInt/" + "match*miss/" + lifeMatchCtr + "*" + lifeMissCtr);

	}
	
	@Override
	public void onPause() {
		super.onPause();
		// Store values between app instances
		// MODE_PRIVATE: file only accessible by calling app (same UID)
		SharedPreferences prefs = getPreferences(MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();

		// get the current values
		Log.i(TAG, "putInt/" + "match*miss/" + lifeMatchCtr + "*" + lifeMissCtr);
		editor.putInt("lifeMatchCtr", lifeMatchCtr);
		editor.putInt("lifeMissCtr", lifeMissCtr);

		// to commit the changes
		editor.commit();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		//Save all the counter values and state of the board
		savedInstanceState.putInt("onGameInvokedCtr", onGameInvokedCtr);
		savedInstanceState.putInt("matchCtr", matchCtr);
		savedInstanceState.putInt("missCtr", missCtr);
		savedInstanceState.putBooleanArray("isClickable", isClickable);
		savedInstanceState.putIntArray("gameBoardLayout", gameBoardLayout);
		savedInstanceState.putIntArray("matchIndex", matchIndex);
		for (int i = 0; i < 9; i++) {
			Log.i(TAG, "onsavestate: "+isClickable[i]+"");
		}
	}

	/**
	 * Initializes and sets up the fields for the image board.
	 * calls Shuffle method and assigns shuffled images to the ImageButtons.
	 */
	public void setupImageLayout() {
		
		onGameInvokedCtr = 0;

		shuffle(rawImageArray);

		for (int i = 0; i < 8; i++) {
			gameBoardLayout[i] = rawImageArray[i];
		}

		int theOne = gameBoardLayout[randomWithRange(0, 7)];

		gameBoardLayout[8] = theOne;

		shuffle(gameBoardLayout);

		int j = 0;
		for (int i = 0; i < 9; i++) {
			if (gameBoardLayout[i] == theOne) {
				matchIndex[j] = i;
				j++;
			}
		}

		for (int i = 0; i < 9; i++) {
			imageButtonArray[i].setClickable(true);
			isClickable[i]=true;
			imageButtonArray[i].setTag(i);
			imageButtonArray[i].setImageResource(gameBoardLayout[i]);
		}
	}
	

	/**
	 * Verifies if the index provided matches one of the duplicate images
	 * 
	 * @param index res id of an image
	 * @return true if index matches a duplicate image
	 * 		false if index does not match
	 */
	private boolean isDuplicateImage(int index) {
		if (index == matchIndex[0]) {
			return true;
		}

		if (index == matchIndex[1]) {
			return true;
		}
		return false;
	}

	/**
	 * Returns a random number that is used for a duplicate image
	 * 
	 * @param min 	the minimum value 
	 * @param max 	the maximum value
	 * @return		random number between min and max
	 */
	private int randomWithRange(int min, int max) {
		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}
	
	/**
	 * Shuffles items in the array
	 * 
	 * @param ar	the array to shuffle
	 */
	private static void shuffle(int[] ar) {
		Random rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--) {
			//get a random number
			int index = rnd.nextInt(i + 1);
			// Simple swap between random number and i decrement
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}

	/**
	 * Called by the scrambled button to reset the board 
	 * 
	 * @param view
	 */
	public void onScramble(View view) {
		setupImageLayout();
	}
	
	/**
	 * Called by any image button to verify if images match or not
	 * 
	 * @param view
	 */
	public void onGame(View view) {

		Log.i(TAG, view.getTag().toString());

		onGameInvokedCtr++;
		view.setClickable(false);
		int index = Integer.parseInt(view.getTag() + "");
		isClickable[index]=false;
		

		if (onGameInvokedCtr % 2 != 0) {
			//if the image is one of the duplicates
			if (isDuplicateImage(index)) {
				gameBoardLayout[index] = back;
				imageButtonArray[index].setImageResource(back);
				Context context = getApplicationContext();
				CharSequence text = getResources().getString(R.string.instruction);
				int duration = Toast.LENGTH_SHORT;
				Toast toast = Toast.makeText(context, text, duration);
				toast.show();
			} else {
				onGameInvokedCtr--;
				missCtr++;
				lifeMissCtr++;
				missesCtr.setText(missCtr + "");
				lifeMissesCtrView.setText(lifeMissCtr + "");

			}
		} else {
			//if the image is one of the duplicates
			if (isDuplicateImage(index)) {
				gameBoardLayout[index] = back;
				imageButtonArray[index].setImageResource(back);
				for (int i = 0; i < 9; i++) {
					imageButtonArray[i].setClickable(false);
					isClickable[i]=false;
				}
				matchCtr++;
				lifeMatchCtr++;
				matchesCtr.setText(matchCtr + "");
				lifeMatchesCtrView.setText(lifeMatchCtr + "");

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

				// set title
				// alertDialogBuilder.setTitle("Your Title");

				// set dialog message
				alertDialogBuilder.setMessage(getResources().getString(R.string.win)).setCancelable(false)
						.setNegativeButton("Continue", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
							}
						});
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			} else {
				onGameInvokedCtr--;
				missCtr++;
				lifeMissCtr++;
				missesCtr.setText(missCtr + "");
				lifeMissesCtrView.setText(lifeMissCtr + "");
				Context context = getApplicationContext();
				CharSequence text = getResources().getString(R.string.lose);
				//int duration = Toast.LENGTH_SHORT;
				Toast toast = Toast.makeText(context, text, 1);
				toast.show();
			}
		}

	}

	/**
	 * Called by the rotate button. Rotates the screen
	 * 
	 * @param view
	 */
	public void onRotate(View view) {
		if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		else
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	}

	/**
	 * Called by zero button, resets the local (current game's) counters
	 * 
	 * @param view
	 */
	public void onZero(View view) {
		matchCtr = 0;
		missCtr = 0;
		matchesCtr.setText(matchCtr + "");
		missesCtr.setText(missCtr + "");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Starts new activity, opens about page
	 */
	public void onAbout(View view){
		startActivity(new Intent(MainActivity.this, About.class));
		
	}
	
}
